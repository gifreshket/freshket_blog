<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include 'include/head_html.php'; ?>
    </head>
    <body>
    	<?php $page = 'about'; ?>
        <?php include 'include/header.php'; ?>
        <section class="page-about">
          <div class="container">
         <h1>เกี่ยวกับเรา</h1>
    		<div>
    		<h3>Freshket คือ "แหล่งรวมซัพพลายเออร์ของสดสำหรับร้านอาหาร"</h3>
        <p class="thai-content">
    		เป้าหมายหลักของเรา คือการช่วยให้ซัพพลายเออร์และร้านอาหารได้พบปะและตกลงซื้อขาย
			ได้ง่ายขึ้น ผ่านระบบการจัดการที่มีประสิทธิภาพ สะดวก รวดเร็ว ปลอดภัย
			ใช้ง่าย ซึ่งจะช่วยประหยัดเวลาและต้นทุน รวมถึงสร้างโอกาสทางธุรกิจใหม่ๆ ให้กับทั้งสองฝ่าย
			เบื้องหลังของ Freshket คือคนคิดนอกกรอบที่อยากทำให้ชีวิตในโลกธุรกิจอาหารที่ช่างยุ่งเหยิงนี้ง่ายลง
			และเราก็ออกไปทำให้มันง่ายลงจริงๆและจะยังคงเดินหน้าพัฒนาต่อไปเพื่อให้ Freshket ยังคงสดใหม่น่าใช้อยู่เสมอ
      </p>
      <p class="before-eng-content">
			Freshket is the marketplace for fresh foods suppliers and restaurants.
      </p>
      <p class="eng-content">
      Our goal is to help suppliers and restaurants find one another and buy/sell fresh ingredients more easily.
			Freshket’s order management system is efficient, convenient, fast, safe and easy to use,
			which helps save time and cost as well as create new business opportunities for both parties.
			Behind Freshket are out-of-the-box thinkers who want to simplify life in this complicated business of food.
			And we actually go out and make it simpler, and will continue to do so to keep Freshket fresh and user-friendly always.
      </p>
    		</div>
        <div class="social-link">
        <a class="facebook"></a>
        <a class="twitter"></a>
        <a class="instagram"></a>
      </div>
      </div>
        </section>
        <?php include 'include/footer.php'; ?>
        <?php include 'include/scripts.php'; ?>
    </body>
</html>
