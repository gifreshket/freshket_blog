<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include 'include/head_html.php'; ?>
    </head>
    <body>
        <?php $page = 'contact'; ?>
        <?php include 'include/header.php'; ?>
        <section class="page-contact">
            <div class="container">
            <h1>ติดต่อเรา</h1>
            <div class="contact-content">
                <div class="content">
                  <p class="contact-into">
                    ติดต่อได้ทุกวัน ตั้งแต่เวลา 8:00 น. - 22:00 น.
                  </p>
                  <p class="contact-telmail">
                    เบอร์ 06-3598-9969</br>
                    อีเมล์ <a href="mailto:contact@freshket.co">contact@freshket.co</a></br>
                  </p>
                  <p class="contact-social">
                    <a href="#" target="_blank">Facebook/FreshKet.co</a></br>
                    <a href="#" target="_blank">Instagram/FreshKet.co</a></br>
                    <a href="#" target="_blank">Twitter/FreshKet.co</a></br>
                    Line@FreshKet</br>
                  </p>
                  <p class="contact-address">
                    เลขที่ 319 อาคารจตุรัสจามจุรี ชั้น 2</br>
                    ถนนพญาไท แขวงปทุมวัน เขตปทุมวัน</br>
                    กรุงเทพฯ 10330</br>
                  </p>
                </div>
                <div class="map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.7637126222635!2d100.52802411298087!3d13.732750501438922!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f2ac59668f9%3A0xad0af01780499bd4!2sChamchuri+Square!5e0!3m2!1sth!2sth!4v1467899987570" frameborder="0" height="100%" width="100%"></iframe>
                </div>
            </div>
          </div>
        </section>
        <?php include 'include/footer.php'; ?>
        <?php include 'include/scripts.php'; ?>
    </body>
</html>
