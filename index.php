<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include 'include/head_html.php'; ?>
    </head>
    <body>
        <?php $page = 'bloglist'; ?>
        <?php include 'include/header.php'; ?>
        <section class="blog-list">
        <?php
        for($i=0;$i<5;$i++){
        ?>
            <div class="blog-item blog-<?php echo ($i%2==0)?"even":"odd"; ?>">
                <div class="container">
                    <div class="white-bg">
                        <div class="blog-thumb">
                            <a href="#"><img src="assets/imgs/blog/thumb_0<?php echo ($i+1); ?>.jpg" /></a>
                            <div class="blog-circle-group">
                                <a href="detail.html" class="btn-circle btn-plus"></a>
                                <a href="#" class="btn-circle btn-share"></a>
                            </div>
                        </div>
                        <div class="blog-content">
                            <h2><a href="detail.html">สเต็ก...เนื้อส่วนใด กินแล้วต้องบอกต่อ!</a></h2>
                            <p>สเต็กหนึ่งจานประกอบด้วยส่วนผสม
                                หลายอย่างแต่หัวใจของสเต็กก็ คือ
                                ก้อนเนื้อส่วนอย่างอื่นในจานไม่ว่าจะเป็น
                                เฟรนฟรายด์หรือมันบด เป็นแค่องเคียงก้อนเนื้
                                นั่นแหละของอร่อยที่แท้จริง</p>
                            <div class="sociallist">
                                <a href="#" class="icon icon-facebook"></a>
                                <a href="#" class="icon icon-twitter"></a>
                                <a href="#" class="icon icon-instagram"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
        <div class="list-pager">
            <ul>
                <li><a href="#" title="prev">&lt;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>
        </div>
        </section>
        <?php include 'include/subscribe.php'; ?>
        <?php include 'include/footer.php'; ?>
        <?php include 'include/scripts.php'; ?>
    </body>
</html>