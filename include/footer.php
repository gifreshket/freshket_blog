        <footer>
            <div id="footer-top">
                <div class="container">
                    <div class="footer-logo">
                        <img src="assets/imgs/masterpage/logo-footer.png" alt="freshket" />
                    </div>
                    <div class="footer-contact">
                        <h2>ติดต่อเฟรชเก็ต</h2>
                        <ul>
                            <li><i></i> ติดต่อได้ทุกวัน ตั้งแต่เวลา 8:00 น. - 22:00 น.</li>
                            <li><i></i> เบอร์ +66 82 442 9515</li>
                            <li><i></i> อีเมล์ <a href="mailto:contact@freshket.co">contact@freshket.co</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="footer-bottom">
                <div class="container">
                    <nav id="footer-menu">
                        <ul>
                            <li><a href="about.html">เกี่ยวกับเรา</a></li>
                            <li><a href="contact.html">ติดต่อเรา</a></li>
                            <li><a href="#">นโยบายความเป็นส่วนตัว</a></li>
                            <li><a href="#">ข้อตกลงและเงื่อนไข</a></li>
                        </ul>
                    </nav>
                    <div class="reserve-text">Forever FRESHKET LIMITED. All Rights Reserved</div>
                </div>
            </div>
        </footer>