        <header class="header-<?php echo $page ?>">
            <div class="container">
                <a class="logo" href="http://freshket.co"></a>
                <button id="showMenu"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                <?php include 'include/navigate.php'; ?>
                <?php if($page == 'bloglist'){ ?>
                <div class="hero-header">
                    <h1>Hello FreshKeters</h1>
                    <p><span>แหล่งรวมเรื่องราวเจาะลึกหลากหลายมุม เปิดโลกตลาดสินค้าสด</span>
    <span>และเบื้องหลังร้านอาหาร ที่คุณอาจไม่เคยรู้มาก่อน</span>
    <span>ติดตามเรื่องราวในวงการของสด แบบสดสด ที่นี่ที่เดียว</span></p>
                </div>
                <a href="#" class="btnScrollDown"></a>
                <?php } ?>
            </div>
        </header>