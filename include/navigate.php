                <nav id="main-menu">
                    <ul>
                        <li><a href="#">หน้าแรก</a></li>
                        <li><a <?php echo ($page == 'bloglist' || $page == 'blogdetail')?'class="active"':''; ?> href="index.html">บล็อค</a></li>
                        <li><a <?php echo ($page == 'about')?'class="active"':''; ?> href="about.html">เกี่ยวกับเรา</a></li>
                        <li><a <?php echo ($page == 'contact')?'class="active"':''; ?> href="contact.html">ติดต่อเรา</a></li>
                        <li><a href="#">เข้าสู่ระบบ</a></li>
                        <li><a href="#" class="menu-register">สมัครสมาชิก</a></li>
                    </ul>
                </nav>