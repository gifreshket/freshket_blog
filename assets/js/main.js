var mainMenu = document.getElementById('main-menu'),
	showMenuBtn = document.getElementById( 'showMenu' ),
	body = document.body;
showMenuBtn.onclick = function() {
	classie.toggle( this, 'active' );
	classie.toggle( body, 'menu-push-toleft' );
	classie.toggle( mainMenu, 'menu-open' );
};
$(document).ready(function(){
	$('.btnScrollDown').on('click', function(){
		$('html, body').scrollTo('.blog-list', 800);
	})
});